
from ngs_arb import Whitw as arb_Whitw
from ngs_arb import Whitw_deriv as arb_Whitw_deriv

# for evaluating whittaker function with high precision 
from mpmath import mpc
from mpmath import whitw as mp_whitw
from mpmath import sqrt as mp_sqrt
import mpmath as mp
mp.mp.dps = 50
maxprec = 40000
maxterms = 10**8
prec = 1000

#kappa  = 33.0+54.5543j
#mu = 98.333-23.123j
#z = 31.45 + 99.567j


kappa  = 3.0+4.14j 
mu = 9.333 -1.3j
z = 3.45 + 9.567j

arb_result = arb_Whitw(kappa,mu,z,prec)
print("arb result = ", arb_result)

mp_result = mp_whitw(kappa,mu,z, maxprec=maxprec,maxterms=maxterms  )
print("mpmath result = ", mp_result)

diff = abs(arb_result-complex(mp_result))
print("abs(diff) =", diff)


mp_result_deriv = (mpc(0.5)- (mpc(mu)-mpc(0.5))/z)*mp_whitw(kappa,mu,z,maxprec=maxprec,maxterms=maxterms)-(1/mp_sqrt(z))*mp_whitw(mpc(kappa)+mpc(0.5),mu-mpc(0.5),z, maxprec=maxprec,maxterms=maxterms )  

arb_result_deriv = arb_Whitw_deriv(kappa,mu,z) 

print("arb deriv result = ", arb_result_deriv)
print("mpmath deriv result = ", mp_result_deriv)
diff_deriv = abs(arb_result_deriv-complex(mp_result_deriv))
print("abs(diff) =", diff_deriv)

kappas = [] 
mus = []
zs = [] 
for i in range(10):
    kappas.append(kappa+i*1j)
    mus.append(mu-i*1j)
    zs.append(z)
arb_result = arb_Whitw_deriv(kappas,mus,zs,prec)
print("arb_list_result = ", arb_result) 



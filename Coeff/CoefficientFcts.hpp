#ifndef __FILE_COEFFICIENTFCTS_HPP
#define __FILE_COEFFICIENTFCTS_HPP

namespace ngfem
{
  
  class NGS_DLL_HEADER WhitwRefsolCoefficientFunction : public CoefficientFunction
  {
    Complex kappa;
    Complex mu;
  public:
    ///
    WhitwRefsolCoefficientFunction (Complex akappa,Complex amu) : CoefficientFunction(1), kappa(akappa), mu(amu) {;}
    ///
    virtual ~WhitwRefsolCoefficientFunction () {}
    ///
    virtual double Evaluate (const BaseMappedIntegrationPoint & ip) const override {       
       
	 
	double x = ip.GetPoint()(0);
	double y = ip.GetPoint()(1);

	return 0.0; 
    }  
   virtual double EvaluateConst () const {
      throw Exception("not constant");
   }
   virtual void PrintReport (ostream & ost) const {
    ost << "Reference solution for mu = "  << mu << endl;
  }
  };

  void Export_whitw_refsol(py::module m);
};

#endif //  __FILE_COEFFICIENTFCTS_HPP


#include <fem.hpp>
//#include <comp.hpp>
#include <python_ngstd.hpp>
#include "CoefficientFcts.hpp"
// #include "arb.h"

namespace ngfem
{
  // Export cf to Python
 
  void Export_whitw_refsol(py::module m)
  {
    m.def("WhitwRefsol", [](Complex kappa,Complex mu) -> shared_ptr<CoefficientFunction>
    {
     return shared_ptr<CoefficientFunction>(make_shared<WhitwRefsolCoefficientFunction> (kappa,mu));
    });
  }
  
  // Register cf for pickling/archiving
  // Create static object with template parameter function and base class.
  //static RegisterClassForArchive<WhitwRefsolCoefficientFunction, CoefficientFunction> regWhitwRefsol;
}

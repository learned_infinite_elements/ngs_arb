#ifndef __FILE_ARBWRAPPER_HPP
#define __FILE_ARBWRAPPER_HPP

#include <solve.hpp>
#include "acb.h"
#include "acb_hypgeom.h"

using namespace ngsolve;

void whitw(acb_t res, const acb_t kappa, const acb_t mu, const acb_t z, slong prec); 
void whitw_deriv(acb_t res, const acb_t kappa, const acb_t mu, const acb_t z, slong prec); 
Complex whitw(Complex akappa, Complex amu, Complex az, slong prec); 
Complex whitw_deriv(Complex akappa, Complex amu, Complex az, slong prec); 
Complex dwhitw_over_whitw(Complex akappa, Complex amu, Complex az, slong prec); 

// Complex whitw_deriv(Complex akappa, Complex amu, Complex az, slong prec); 

#endif //  __FILE_ARBWRAPPER_HPP

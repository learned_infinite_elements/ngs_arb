#include "arb_wrapper.hpp"
// #include "acb.h"
// #include "acb_hypgeom.h"

using namespace ngsolve;

void whitw(acb_t res, const acb_t kappa, const acb_t mu, const acb_t z, slong prec) {

  acb_t a,b,tmp,tmp1;
  acb_init(a);
  acb_init(b);
  acb_init(tmp);
  acb_init(tmp1);

  acb_sub(a,mu,kappa,prec) ;
  acb_set_d_d(tmp,0.5,0.0);
  acb_add(a,a,tmp,prec);

  acb_set_d_d(tmp,1.0,0.0);
  acb_add(b,mu,mu,prec);
  acb_add(b,b,tmp,prec);

  acb_hypgeom_u(res,a,b,z,prec);
    
  acb_set_d_d(tmp,-0.5,0.0);
  acb_mul(tmp1,z,tmp,prec);
  acb_exp(tmp,tmp1,prec);
  acb_mul(res,res,tmp,prec);

  acb_set_d_d(tmp,0.5,0.0);
  acb_add(tmp1,mu,tmp,prec);
  acb_pow(tmp,z,tmp1,prec);
  acb_mul(res,res,tmp,prec);

  acb_clear(a);  
  acb_clear(b);
  acb_clear(tmp);
  acb_clear(tmp1);

}	

void whitw_deriv(acb_t res, const acb_t kappa, const acb_t mu, const acb_t z, slong prec) {
  
  acb_t kappa_shift,mu_shift,tmp,tmp1;
  acb_init(kappa_shift);
  acb_init(mu_shift);
  acb_init(tmp);
  acb_init(tmp1);

  acb_set_d_d(tmp,0.5,0.0);
  acb_add(kappa_shift,kappa,tmp,prec);
  acb_sub(mu_shift,mu,tmp,prec);
  
  acb_div(tmp1,mu_shift,z,prec);
  acb_sub(tmp,tmp,tmp1,prec); 
  
  whitw(res,kappa,mu,z,prec); 
  acb_mul(res,tmp,res,prec); 

  whitw(tmp,kappa_shift,mu_shift,z,prec); 
  acb_rsqrt(tmp1,z,prec);
  acb_mul(tmp,tmp1,tmp,prec);
  
  acb_sub(res,res,tmp,prec);
	
  acb_clear(kappa_shift);  
  acb_clear(mu_shift);  
  acb_clear(tmp);  
  acb_clear(tmp1);  

}



Complex whitw(Complex akappa, Complex amu, Complex az, slong prec) {

  Complex result; 
  
  acb_t res,kappa,mu,z;
  acb_init(kappa);
  acb_init(mu);
  acb_init(z);
  acb_init(res);

  acb_set_d_d(kappa,akappa.real(),akappa.imag());
  acb_set_d_d(mu,amu.real(),amu.imag());
  acb_set_d_d(z,az.real(),az.imag());
     
  whitw(res,kappa,mu,z,prec); 
  
  double re,im; 
  re = arf_get_d(arb_midref(acb_realref(res)), ARF_RND_NEAR);  
  im = arf_get_d(arb_midref(acb_imagref(res)), ARF_RND_NEAR);
  result = Complex(re,im);

  acb_clear(kappa);
  acb_clear(mu);
  acb_clear(z);
  acb_clear(res);

  return result; 
    
}	

Complex whitw_deriv(Complex akappa, Complex amu, Complex az, slong prec) {

  Complex result; 
  
  acb_t res,kappa,mu,z;
  acb_init(kappa);
  acb_init(mu);
  acb_init(z);
  acb_init(res);

  acb_set_d_d(kappa,akappa.real(),akappa.imag());
  acb_set_d_d(mu,amu.real(),amu.imag());
  acb_set_d_d(z,az.real(),az.imag());
     
  whitw_deriv(res,kappa,mu,z,prec); 
  
  double re,im; 
  re = arf_get_d(arb_midref(acb_realref(res)), ARF_RND_NEAR);  
  im = arf_get_d(arb_midref(acb_imagref(res)), ARF_RND_NEAR);
  result = Complex(re,im);

  acb_clear(kappa);
  acb_clear(mu);
  acb_clear(z);
  acb_clear(res);

  return result; 
  
}	

Complex dwhitw_over_whitw(Complex akappa, Complex amu, Complex az, slong prec) {

  Complex result; 
  
  acb_t res,kappa,mu,z,res_whitw,res_dwhitw;
  acb_init(kappa);
  acb_init(mu);
  acb_init(z);
  acb_init(res);
  acb_init(res_whitw);
  acb_init(res_dwhitw);

  acb_set_d_d(kappa,akappa.real(),akappa.imag());
  acb_set_d_d(mu,amu.real(),amu.imag());
  acb_set_d_d(z,az.real(),az.imag());
     
  whitw(res_whitw,kappa,mu,z,prec); 
  whitw_deriv(res_dwhitw,kappa,mu,z,prec);  
  acb_div(res,res_dwhitw,res_whitw,prec);

  double re,im; 
  re = arf_get_d(arb_midref(acb_realref(res)), ARF_RND_NEAR);  
  im = arf_get_d(arb_midref(acb_imagref(res)), ARF_RND_NEAR);
  result = Complex(re,im);

  acb_clear(kappa);
  acb_clear(mu);
  acb_clear(z);
  acb_clear(res);
  acb_clear(res_whitw);
  acb_init(res_dwhitw);

  return result; 
  
}	

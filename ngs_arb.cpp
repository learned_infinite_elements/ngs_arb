#include <solve.hpp>
using namespace ngsolve;
using namespace ngfem;
#include <python_comp.hpp>
#include "Coeff/CoefficientFcts.hpp"
#include "Coeff/arb_wrapper.hpp"
#include "acb.h"
#include <omp.h>
// #include "acb_hypgeom.h"


PYBIND11_MODULE(ngs_arb,m) {
  // import ngsolve such that python base classes are defined
  auto ngs = py::module::import("ngsolve");
 
  static size_t global_heapsize = 1000000;
  static LocalHeap glh(global_heapsize, "ngs_arb", true);
 

  Export_whitw_refsol(m);
   
  m.def("Whitw",[] (Complex akappa,
    		    Complex amu,
	            Complex az,	    
		    int precision
		   ) {

    slong prec = precision; 
    py::object result; 
    result = py::cast( whitw(akappa,amu,az,prec) );  
    return result; 

  },
  py::arg("kappa"),py::arg("mu"),py::arg("z"),py::arg("precision")=1000 
  ); 
   
  m.def("Whitw",[] (py::list kappas,
    		    py::list mus,
	            py::list zs,	    
		    int precision
		   ) {

    slong prec = precision; 
    py::list result; 
    for(int j = 0; j < py::len(kappas); j++) {
      Complex kappa = py::cast<Complex>(kappas[j]);
      Complex mu = py::cast<Complex>(mus[j]); 
      Complex z = py::cast<Complex>(zs[j]); 
      result.append( py::cast(whitw(kappa,mu,z,prec)));
    }  
    return result; 

  },
  py::arg("kappas"),py::arg("mus"),py::arg("zs"),py::arg("precision")=1000 
  ); 

  m.def("Whitw_deriv",[] (Complex akappa,
    		          Complex amu,
	                  Complex az,	    
		          int precision
		         ) {

    slong prec = precision; 
    py::object result; 
    result = py::cast( whitw_deriv(akappa,amu,az,prec) );  
    return result; 

  },
  py::arg("kappa"),py::arg("mu"),py::arg("z"),py::arg("precision")=1000 
  );

  m.def("Whitw_deriv",[] (py::list kappas,
    		          py::list mus,
	                  py::list zs,	    
		          int precision
		        ) {

    slong prec = precision; 
    py::list result; 
    for(int j = 0; j < py::len(kappas); j++) {
      Complex kappa = py::cast<Complex>(kappas[j]);
      Complex mu = py::cast<Complex>(mus[j]); 
      Complex z = py::cast<Complex>(zs[j]); 
      result.append( py::cast(whitw_deriv(kappa,mu,z,prec)));
    }  
    return result; 

  },
  py::arg("kappas"),py::arg("mus"),py::arg("zs"),py::arg("precision")=1000 
  ); 

  m.def("Whitw_deriv_over_Whitw",[] (py::list kappas,
    		                     py::list mus,
	                             py::list zs,	    
		                     int precision
		                    ) {

    slong prec = precision; 
    py::list result; 
    for(int j = 0; j < py::len(kappas); j++) {
      Complex kappa = py::cast<Complex>(kappas[j]);
      Complex mu = py::cast<Complex>(mus[j]); 
      Complex z = py::cast<Complex>(zs[j]); 

      result.append( py::cast(dwhitw_over_whitw(kappa,mu,z,prec)));
    }  
    return result; 

  },
  py::arg("kappas"),py::arg("mus"),py::arg("zs"),py::arg("precision")=1000 
  ); 

  m.def("Whitw_deriv_over_Whitw_par",[] (py::list kappas,
    		                     py::list mus,
	                             py::list zs,	    
		                     int precision
		                    ) {

    slong prec = precision; 
    Vector<Complex> vals(py::len(kappas));
    Vector<Complex> kappa_v(py::len(kappas));
    Vector<Complex> mu_v(py::len(kappas));
    Vector<Complex> z_v(py::len(kappas));
    for(int j = 0; j < py::len(kappas); j++) {
      kappa_v(j) = py::cast<Complex>(kappas[j]);
      mu_v(j) = py::cast<Complex>(mus[j]); 
      z_v(j) = py::cast<Complex>(zs[j]); 
    }
    #pragma omp parallel for shared(vals,kappa_v,mu_v,z_v,precision)
    for(int j = 0; j < py::len(kappas); j++) {
      //std::cout << "j = " << j << std::endl;
      //Complex kappa = py::cast<Complex>(kappas[j]);
      //Complex mu = py::cast<Complex>(mus[j]); 
      //Complex z = py::cast<Complex>(zs[j]); 
       vals(j) = dwhitw_over_whitw(kappa_v(j),mu_v(j),z_v(j),prec);
    }
    
    py::list result; 
    for(int j= 0; j < py::len(kappas);j++) {
      result.append(vals(j));
    }

    return result; 
  },
  py::arg("kappas"),py::arg("mus"),py::arg("zs"),py::arg("precision")=1000 
  ); 
 
  m.def("Whitw_deriv_over_Whitw_spar",[] (Complex kappa,
    		                          Complex z,
	                                  int Lmax,	    
		                          int precision
		                         ) {

    //Complex kappa = py::cast<Complex>(a_kappa);
    //Complex z = py::cast<Complex>(a_z);
    slong prec = precision; 
    Vector<Complex> vals(Lmax+1);
    #pragma omp parallel for shared(vals,kappa,z,Lmax,prec)
    for(int l = 0; l < Lmax + 1; l++) {
       vals(l) = dwhitw_over_whitw(kappa,0.5+l,z,prec);
    }
    
    py::list result; 
    for(int l= 0; l < Lmax+1;l++) {
      result.append(vals(l));
    }

    return result; 
  },
  py::arg("kappa"),py::arg("Lmax"),py::arg("z"),py::arg("precision")=1000 
  ); 
  // This adds documented flags to the docstring of objects.
  ngs.attr("_add_flags_doc")(m);
}

